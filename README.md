Unicode Info
============

A short program to show some details about unicode (UTF-8) text input.

Written mostly for myself, so I can quickly get some info about specific unicode strings. The output is rather crude, so is the code.

It prints out the following information:

- a list of the unicode code points (with names)
- the ascii characters in the string (if applicable)
- the graphemes (multiple code points that are rendered as one glyph)
- the UTF-8 bytes in binary
- the UTF-16 code units in binary
- the code points normalized with four different normalization methods (nfc, nfd, nfkc, nfkd)

Usage
-----

Run with cargo or build and run manually. use the text you want to analyze as CLI param.
```
cargo run "pi 🏴‍☠️"
```

The input above would print the output:

```
Text: 'pi 🏴‍☠️'
7 code points: U+0070 (LATIN SMALL LETTER P), U+0069 (LATIN SMALL LETTER I), U+0020 (SPACE), U+1F3F4 (WAVING BLACK FLAG), U+200D (ZERO WIDTH JOINER), U+2620 (SKULL AND CROSSBONES), U+FE0F (VARIATION SELECTOR-16)
ascii: 'p' 0x70, 'i' 0x69, ' ' 0x20, n/a, n/a, n/a, n/a
4 graphemes: 'p' U+0070 (LATIN SMALL LETTER P), 'i' U+0069 (LATIN SMALL LETTER I), ' ' U+0020 (SPACE), '🏴‍☠️' U+1F3F4 (WAVING BLACK FLAG), U+200D (ZERO WIDTH JOINER), U+2620 (SKULL AND CROSSBONES), U+FE0F (VARIATION SELECTOR-16)
16 bytes, utf-8: (01110000), (01101001), (00100000), (11110000, 10011111, 10001111, 10110100), (11100010, 10000000, 10001101), (11100010, 10011000, 10100000), (11101111, 10111000, 10001111)
8 16-bit units, utf-16: (0000000001110000), (0000000001101001), (0000000000100000), (1101100000111100, 1101111111110100), (0010000000001101), (0010011000100000), (1111111000001111)
normalized text (nfc): pi 🏴‍☠️
7 normalized code points(nfc): U+0070 (LATIN SMALL LETTER P), U+0069 (LATIN SMALL LETTER I), U+0020 (SPACE), U+1F3F4 (WAVING BLACK FLAG), U+200D (ZERO WIDTH JOINER), U+2620 (SKULL AND CROSSBONES), U+FE0F (VARIATION SELECTOR-16)
normalized text (nfd): pi 🏴‍☠️
7 normalized code points(nfd): U+0070 (LATIN SMALL LETTER P), U+0069 (LATIN SMALL LETTER I), U+0020 (SPACE), U+1F3F4 (WAVING BLACK FLAG), U+200D (ZERO WIDTH JOINER), U+2620 (SKULL AND CROSSBONES), U+FE0F (VARIATION SELECTOR-16)
normalized text (nfkc): pi 🏴‍☠️
7 normalized code points(nfkc): U+0070 (LATIN SMALL LETTER P), U+0069 (LATIN SMALL LETTER I), U+0020 (SPACE), U+1F3F4 (WAVING BLACK FLAG), U+200D (ZERO WIDTH JOINER), U+2620 (SKULL AND CROSSBONES), U+FE0F (VARIATION SELECTOR-16)
normalized text (nfkd): pi 🏴‍☠️
7 normalized code points(nfkd): U+0070 (LATIN SMALL LETTER P), U+0069 (LATIN SMALL LETTER I), U+0020 (SPACE), U+1F3F4 (WAVING BLACK FLAG), U+200D (ZERO WIDTH JOINER), U+2620 (SKULL AND CROSSBONES), U+FE0F (VARIATION SELECTOR-16)
```
