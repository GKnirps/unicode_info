#![forbid(unsafe_code)]

use std::env;
use unicode_names2::name;
use unicode_normalization::UnicodeNormalization;
use unicode_properties::{GeneralCategory, UnicodeGeneralCategory};
use unicode_segmentation::UnicodeSegmentation;

fn main() -> Result<(), String> {
    let input = env::args()
        .nth(1)
        .ok_or_else(|| "Missing parameter".to_owned())?;

    let nfc: String = input.nfc().collect();
    let nfd: String = input.nfd().collect();
    let nfkc: String = input.nfkc().collect();
    let nfkd: String = input.nfkd().collect();

    println!(
        "Text: '{}'\n\
        utf-8 text in hexadecimal: '{:02x?}'\n\
        {} code points: {}\n\
        ascii: {}\n\
        {} graphemes: {}\n\
        {} bytes, utf-8: {}\n\
        {} 16-bit units, utf-16: {}\n\
        normalized text (nfc): {}\n\
        {} normalized code points(nfc): {}\n\
        normalized text (nfd): {}\n\
        {} normalized code points(nfd): {}\n\
        normalized text (nfkc): {}\n\
        {} normalized code points(nfkc): {}\n\
        normalized text (nfkd): {}\n\
        {} normalized code points(nfkd): {}",
        input,
        input.as_bytes(),
        input.chars().count(),
        code_point_list(&input),
        ascii_elements(&input),
        input.graphemes(true).count(),
        graphemes(&input),
        input.len(),
        utf8_binary(&input),
        len_utf16(&input),
        utf16_binary(&input),
        nfc,
        nfc.chars().count(),
        code_point_list(&nfc),
        nfd,
        nfd.chars().count(),
        code_point_list(&nfd),
        nfkc,
        nfkc.chars().count(),
        code_point_list(&nfkc),
        nfkd,
        nfkd.chars().count(),
        code_point_list(&nfkd),
    );

    Ok(())
}

fn graphemes(s: &str) -> String {
    let g: Vec<String> = s
        .graphemes(true)
        .map(|g| format!("'{}' {}", g, code_point_list(g)))
        .collect();
    g.join(", ")
}

fn ascii_elements(s: &str) -> String {
    let elements: Vec<String> = s
        .chars()
        .map(|c| {
            if c.is_ascii() {
                let mut buf = [0; 1];
                format!("'{}' 0x{:02x}", c, c.encode_utf8(&mut buf).as_bytes()[0])
            } else {
                "n/a".to_owned()
            }
        })
        .collect();
    elements.join(", ")
}

fn unicode_category(c: char) -> &'static str {
    match c.general_category() {
        GeneralCategory::UppercaseLetter => "Lu",
        GeneralCategory::LowercaseLetter => "Ll",
        GeneralCategory::TitlecaseLetter => "Lt",
        GeneralCategory::ModifierLetter => "Lm",
        GeneralCategory::OtherLetter => "Lo",
        GeneralCategory::NonspacingMark => "Mn",
        GeneralCategory::SpacingMark => "Mc",
        GeneralCategory::EnclosingMark => "Me",
        GeneralCategory::DecimalNumber => "Nd",
        GeneralCategory::LetterNumber => "Nl",
        GeneralCategory::OtherNumber => "No",
        GeneralCategory::ConnectorPunctuation => "Pc",
        GeneralCategory::DashPunctuation => "Pd",
        GeneralCategory::OpenPunctuation => "Ps",
        GeneralCategory::ClosePunctuation => "Pe",
        GeneralCategory::InitialPunctuation => "Pi",
        GeneralCategory::FinalPunctuation => "Pf",
        GeneralCategory::OtherPunctuation => "Po",
        GeneralCategory::MathSymbol => "Sm",
        GeneralCategory::CurrencySymbol => "Sc",
        GeneralCategory::ModifierSymbol => "Sk",
        GeneralCategory::OtherSymbol => "So",
        GeneralCategory::SpaceSeparator => "Zs",
        GeneralCategory::LineSeparator => "Zl",
        GeneralCategory::ParagraphSeparator => "Zp",
        GeneralCategory::Control => "Cc",
        GeneralCategory::Format => "Cf",
        GeneralCategory::Surrogate => "Cs", // I think this should not occur, it's illegal to encode in utf-8
        GeneralCategory::PrivateUse => "Co",
        GeneralCategory::Unassigned => "Cn",
    }
}

fn code_point_unicode(c: char) -> String {
    format!(
        "{} U+{:04X} ({})",
        unicode_category(c),
        c as u32,
        name(c)
            .map(|n| n.to_string())
            .unwrap_or_else(|| "<unknown>".to_string())
    )
}

fn code_point_list(s: &str) -> String {
    let codepoints: Vec<String> = s.chars().map(code_point_unicode).collect();
    codepoints.join(", ")
}

fn code_point_utf8(c: char) -> String {
    let mut buff = [0; 4];
    let s = c.encode_utf8(&mut buff);
    let bytes: Vec<String> = s.bytes().map(|b| format!("{:08b}", b)).collect();
    bytes.join(", ")
}

fn utf8_binary(s: &str) -> String {
    let codepoints: Vec<String> = s
        .chars()
        .map(|c| format!("({})", code_point_utf8(c)))
        .collect();
    codepoints.join(", ")
}

fn code_point_utf16(c: char) -> String {
    let mut buff: [u16; 2] = [0, 2];
    let s = c.encode_utf16(&mut buff);
    let units: Vec<String> = s.iter().map(|u| format!("{:016b}", u)).collect();
    units.join(", ")
}

fn utf16_binary(s: &str) -> String {
    let codepoints: Vec<String> = s
        .chars()
        .map(|c| format!("({})", code_point_utf16(c)))
        .collect();
    codepoints.join(", ")
}

fn len_utf16(s: &str) -> usize {
    s.chars().map(char::len_utf16).sum()
}
